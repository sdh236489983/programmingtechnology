#include "Board.h"
#include <stdio.h>

/*
* Initialize the board
*/
void initializeBoard(boardGame * game) {
		//Clear the board
	for (int i = 0; i < BOARD_ROW; i++) {
		for (int j = 0; j < BOARD_COLUMN; j++) {
			game->position[i][j] = '-';
		}
	}

		//Add some random treasures
	int numTreasures = (rand() % (BOARD_ROW*BOARD_COLUMN))/5;
	for (int i = 0; i < numTreasures; i++) {
		game->position[rand() % BOARD_ROW][rand() % BOARD_COLUMN] = '#';
	}
		
		//Initialize the players information
	for (int i = 0; i < MAX_PLAYERS; i++) {
		printf("Player %d, what's your name?", i);
		scanf("%s", game->players[i].name);
		game->players[i].score = 0;
		game->players[i].xPosition = rand() % BOARD_ROW;
		game->players[i].yPosition = rand() % BOARD_COLUMN;
	}

	game->currentPlayer = rand()%MAX_PLAYERS;
	game->pendingMovements = (rand() % (BOARD_ROW*BOARD_COLUMN))/6;
	game->gameState = PLAY;
}

/*
* Update the current player's position
*/
void updatePlayerPosition(boardGame *game, char move) {
	int x, y;
	x = game->players[game->currentPlayer].xPosition;
	y = game->players[game->currentPlayer].yPosition;
	

	switch (move) 	{
		case 'w':
			if (x > 0)
				game->players[game->currentPlayer].xPosition = x - 1;
			break;
		case 's':
			if (x < BOARD_ROW-1)
				game->players[game->currentPlayer].xPosition = x + 1;
			break;
		case 'd':
			if (y < BOARD_COLUMN-1)
				game->players[game->currentPlayer].yPosition = y + 1;
			break;
		case 'a':
			if (y > 0)
				game->players[game->currentPlayer].yPosition = y - 1;
		break;
	}
}

/*
* Execute the game logic: 
* - A player increases their score if it is on '#' cell 
* - A player throws their opponent to a random position when they share the same position. 
	Moreover, the current player increases their score and the opponent's score is decreased.
* - Nothing happens when the player is on a '-' cell
* - The game ends when the number of pending movements is 0
*/
void executeGameLogic(boardGame * game) {
		//Local variables that contains the position of the current player
	int x = game->players[game->currentPlayer].xPosition;
	int y = game->players[game->currentPlayer].yPosition;
	
		//Local variables that contains the position of the opponent
	int opponent = (game->currentPlayer + 1) % MAX_PLAYERS;
	int xOpponent= game->players[opponent].xPosition;
	int yOpponent = game->players[opponent].xPosition;


	if (game->position[x][y] == '#') {
		//Get the treasure and update the score
		game->position[x][y] = '-';
		game->players[game->currentPlayer].score = game->players[game->currentPlayer].score + 3;
	}
	else if ((x==xOpponent)&&(y=yOpponent)) {
		//Both players are sharing the same position so the oldest player in that position is thrown to another position and their score is reduced
		//We "suppose" that, after the random operation, the new position will be different from the current position
		game->players[opponent].xPosition = rand() % BOARD_ROW;
		game->players[opponent].yPosition = rand() % BOARD_COLUMN;
		game->players[opponent].score--;
		game->players[game->currentPlayer].score++;
	}
	//Move to the next turn
	game->currentPlayer = (game->currentPlayer + 1) % MAX_PLAYERS;
	game->pendingMovements--;
	if (game->pendingMovements == 0) {
		game->gameState = EXIT;
	}
}

/*
* Draw the winner based on the current score
*/
void drawWinner(boardGame * game) {
	int winner;
	if (game->players[PLAYER_1].score > game->players[PLAYER_2].score) {
		winner = PLAYER_1;
	}
	else if (game->players[PLAYER_1].score < game->players[PLAYER_2].score) {
		winner = PLAYER_2;
	}
	else {
		winner = 0;
	}

	if (winner == 0) {
		printf("Draw, well played!!!\n");
	}
	else {
		printf("Congratulations %s!!!\n", game->players[winner].name);
	}
	system("pause");
}

/*
* Print the board on the screen
*/
void drawBoard(boardGame *game) {
	printf(" ");
	for (int j = 0; j < BOARD_COLUMN; j++) {
		printf("--");
	}
	printf("\n");
	
	for (int i = 0; i < BOARD_ROW; i++) {
		printf("|");
		for (int j = 0; j < BOARD_COLUMN; j++) {
			if ((game->players[PLAYER_1].xPosition == i) && (game->players[PLAYER_1].yPosition == j)){
				printf(" 1");
			}else if ((game->players[PLAYER_2].xPosition == i) && (game->players[PLAYER_2].yPosition == j)) {
				printf(" 2");
			}else {
				printf(" %c", game->position[i][j]);
			}
		}printf(" |\n");
	}

	printf(" ");
	for (int j = 0; j < BOARD_COLUMN; j++) {
		printf("--");
	}
	printf("\n");

	for (int i = 0; i < MAX_PLAYERS; i++) {
		printf("\t %s 's score: %d", game->players[i].name, game->players[i].score);
	}
	printf("\nPending movements: %d\n", game->pendingMovements);

}



