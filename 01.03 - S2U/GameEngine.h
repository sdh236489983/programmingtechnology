#pragma once

#include "Board.h"

/**
* This module manages the game engine
* Rememer to add _CRT_SECURE_NO_WARNINGS into Properties->C/C++->Preprocessor->Preprocess properties in order to skip the warnings of Visual Studio regarding to scanf and printf
*/


//Start the game's execution
void run();
//Initialize the game's components
void initialize(boardGame * game);
//Game's execution : Gets input events, processes game
//logic and draws the game's output on the screen
void gameLoop(boardGame * game);
//Get and execute the player's events
void eventHandler(boardGame * game);
//Execute the game's physics/logic
void doPhysics(boardGame * game);
//Draw the game's objects on the screen
void renderGame(boardGame * game);



