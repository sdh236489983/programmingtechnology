#include "GameEngine.h"
#include <stdlib.h>
#include <stdio.h>

/*
* Start the game's execution
*/
void run() {
		//Variable declaration
	boardGame game;

		//Initialize all the game components
	initialize(&game);

		//Start the game if everything is ready
	gameLoop(&game);
}

/*
* Initialize the game components
*/
void initialize(boardGame * game) {
	//Prepare system for working with random numbers
	srand(time(NULL));
	//Prepare the board
	initializeBoard(game);
}


/*
* Game execution: Gets input events, processes game logic 
and draws the game's output on the screen
*/
void gameLoop(boardGame * game) {
	while (game->gameState != EXIT) {
			//Render game
		renderGame(game);
			//Detect the player's commands 
		eventHandler(game);
			//Do physics
		doPhysics(game);			
	}
	drawWinner(game);
}

/**
* Get and execute the player's events
* Reserved keys:
- w | a | s | d moves the player object
- e: exit the game
*/
void eventHandler(boardGame * game) {
	char move = '-';	
	printf("%s, what's your next move? ( w | s | d | a | e ): ", game->players[game->currentPlayer].name);
	do {
		scanf("%c", &move);
	} while ((move != 'e') && (move != 'w') && (move != 's') && (move != 'd') && (move != 'a'));

	if (move == 'e') {
		game->gameState = EXIT;	
	}else {
		updatePlayerPosition(game, move);
	}
}

/*
* Execute the game's physics/logic
*/
void doPhysics(boardGame * game) {
	executeGameLogic(game);
}


/**
* Draw the game's objects on the screen
*/
void renderGame(boardGame * game) {
		//Clear the screen	
	system("cls");
		//Draw the board
	drawBoard(game);	
}

int main(int argc, char ** argv) {
	run();	
	return 0;
}


