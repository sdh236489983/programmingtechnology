#pragma once
#include "Person.h"

/*
* This class represents an array of people.
* This implementation is based on allocation the space only when is needed.
* It is an optimal solution from the point of view of the memory usage because it only uses the memory that is needed in each moment.
* It is totally inefficient from the computational point of view due because it has to swap all the content when a new person is added or a person is deleted
*/

class ListOfPeople
{
		//A dynamic array of person's object
	Person * _arrayPerson;
		//The number of people
	int _numPeople;

public:
		//Constructor
	ListOfPeople();
		//Destructor
	~ListOfPeople();
		//Add a person
	void addPerson(Person & p);
		//Remove a person
	void removePerson(int id);
		//Get a person
	Person getPerson(int id);
		//Get the size of the data structure
	int getHowManyPeople();
		//Print the content
	void printList();
};

