#include "ListOfPeople.h"

/*
BE VERY VERY VERY CAREFUL when you copy, pass by value or return objects.
In these cases, you need to implement the object copy constructor.
Try to comment the Person copy constructor and check the values using the debugger. What happens?

IT'S VERY IMPORTANT free the memory that has been previously allocated to avoid memory leaks

Create a console application for:
- Asking data regarding to several people
- Adding two people into the data structure
- Showing the content of the data structure
- Removing a person
- Showing the content of the data structure

Additional exercise: Try to return code errors and process them instead of exiting from the application in the ListOfPeople class
*/

int main() {
	int peopleSize = 3;
	Person p;
	ListOfPeople people;

	//Fill data
	for (int i = 0; i < peopleSize; i++) {
		p.askPersonData();
		people.addPerson(p);
	}

	//We want to add a new person but the space is full
	//In this case, the addPerson method will allocate a new chunk of memory
	Person p2("Albert", 'M', 33);
	people.addPerson(p2);

	people.printList();

	//Let's imagine that we want to remove the person in the first position
	people.removePerson(0);

	//Show the final list
	people.printList();

	system("pause");
	return 0;

}