#include "ListOfPeople.h"
#include <iostream>

/**
* Constructor
*/
ListOfPeople::ListOfPeople() {
	_arrayPerson = 0;
	_numPeople = 0;
}

/**
* Destructor
*/
ListOfPeople::~ListOfPeople() {
	if (_numPeople != 0) {
		delete[] _arrayPerson;
	}
}

/**
* Add a person into the data structure. Thus, it has to ask memory for the new size and copy the previous people
* @param p is the new person to add
*/
void ListOfPeople::addPerson(Person & p) {
	Person * temp;

	temp= new Person[_numPeople + 1];
	if (temp == 0) {
		std::cout << "[ListOfPeople::addPerson] System was not able to allocate memory";
		exit(-1);
	}
	for (int i = 0; i < _numPeople; i++) {
		temp[i] = _arrayPerson[i];
	}
	temp[_numPeople] = p;
	
	if (_numPeople != 0) {
		delete[] _arrayPerson;
	}

	_numPeople++;

	_arrayPerson = temp;
}

/* 
* Removes a person from the data structure
*/
void ListOfPeople::removePerson(int id) {
	//Homework
}

/* 
* Gets a person from the data structure
* @param id is the person to select
*/
Person ListOfPeople::getPerson(int id) {
	return _arrayPerson[id];
}
/* 
* Gets the size of the data structure
* @return the size of the array
*/
int ListOfPeople::getHowManyPeople() {
	return _numPeople;
}

/*
* Print the content of the list
*/
void ListOfPeople::printList() {
	for (int i = 0; i < _numPeople; i++) {
		_arrayPerson[i].printPerson();
	}
}
