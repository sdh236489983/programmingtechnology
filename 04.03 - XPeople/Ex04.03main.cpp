#include "Person.h"
#include <cstdlib>				//Management of system functions such as pause
#define MAX_PEOPLE 4

/*
This program is not controlling user errors.
What happens if you introduce more than one character in the sex field?  Or if the name contains spaces? Or if you write characters instead of numbers in the age?
You will get weird behaviours. Therefore, a right approach should check if data is in the right format before converting to specific data types

YOU HAVE TO UNALLOCATE THE MEMORY from HEAP!!!

*/

int main() {
		//Create a DYNAMIC array
	Person * arrayPeople;
	arrayPeople = new Person[MAX_PEOPLE];

		//Ask data
	for (int i = 0; i < MAX_PEOPLE; i++) {
		arrayPeople[i].askPersonData();
	}
		//Show data
	for (int i = 0; i < MAX_PEOPLE; i++) {
		arrayPeople[i].printPerson();
	}
		//Delete will force system to call the destructor of the objects and, next, system will unallocate memory from Heap
	delete[] arrayPeople;
	system("pause");
	return 0;
}