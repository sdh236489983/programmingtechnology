#include "Person.h"
#include <cstdlib>				//Management of system functions such as pause

/*
  This program is not controlling user errors.
  What happens if you introduce more than one character in the sex field?  Or if the name contains spaces? Or if you write characters instead of numbers in the age?
  You will get weird behaviours. Therefore, a right approach should check if data is in the right format before converting to specific data types
*/

int main() {
		//Call 3 constructors (1 constructor for each object)
	Person p1;
	p1.askPersonData();
	Person p2("Albert", 34, 'M');
	Person p3(p1);

	p1.printPerson();
	p2.printPerson();
	p3.printPerson();

	system("pause");
	return 0;
	//Destructors are automatically called when the method ends
}