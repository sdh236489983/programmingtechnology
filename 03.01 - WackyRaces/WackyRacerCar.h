#pragma once
#include <string>
#define PURPLE_COLOR 1
#define RED_COLOR 2

class WackyRacerCar {
private:		
	int _numberId;		         //Car characteristics
	int _numberOfWheels;
	int _primaryColor;
	std::string _material;
	int _carOccupants;
	std::string _specialSkill;			
	float _currentPetrol;        //Current car's state
	int _currentPosition;
	float _currentSpeed;
	int _currentGear;
	float _currentAngleSteering;
	bool _engineStarted;		
		
	bool computeNewPetrolValue();//Internal methods
public:
		//Default Constructor
	WackyRacerCar();
		//Overload constructor
	WackyRacerCar(int numberId, int numberOfWheels, int primaryColor
				, std::string material, int carOccupants
				, std::string specialSkill, float petrol, int curPosition);
		//Copy constructor
	WackyRacerCar(WackyRacerCar & car1);
		//Destructor
	~WackyRacerCar();		
		//Getter and setter methods
	void setCurrentPetrol(float newPetrol);
	float getCurrentPetrol();				//Both functions do the same
	void getCurrentPetrol(float & returningCurrentPetrol);
		//Car actions methods
	void startEngine(); 	void stopEngine(); 	void accelerate();
	void brake();			void gearUp();   	void gearDown();
	void moveSteering(float angle);
};

