#include "WackyRacerCar.h"
#include <iostream>
#include <cstdlib>

/**
* Default Constructor
*/
WackyRacerCar::WackyRacerCar() {
	_numberId = 0; 	_numberOfWheels = 0; _primaryColor = 0; 
	_material = ""; _carOccupants = 0; 	_specialSkill = "";
	_currentPetrol = 0; _currentPosition = 0; _currentSpeed = 0;
	_currentGear = 0; _currentAngleSteering = 0; 	_engineStarted = false;
	std::cout << "[Default constructor] Car number " << this->_numberId << " is created but its data is empty \n" << std::endl;
}

/**
* Overload constructor
* @param.... all the parameters without the ones that are related to the car state. By default, the car is stopped
*/
WackyRacerCar::WackyRacerCar(int _numberId, int numberOfWheels, int primaryColor,
	std::string material, int carOccupants, std::string specialSkill, float petrol, int currentPosition) {
	this->_numberId = _numberId;
	_numberOfWheels = numberOfWheels; _primaryColor = primaryColor;
	_material = material; 	_carOccupants = carOccupants; 	_specialSkill = specialSkill; _currentPetrol = petrol;
	_currentPosition = 0; _currentSpeed = 0; _currentGear = 0; 	_currentAngleSteering = 0; _engineStarted = false;
	std::cout << "[Overload constructor] Car number " << this->_numberId << " is created with valid data\n" << std::endl;
}

/**
* Copy constructor
* @param car1 is the WackyRacerCar object used to initialize the current object
*/
WackyRacerCar::WackyRacerCar(WackyRacerCar & car1) {
	_numberId = car1._numberId; 	_numberOfWheels = car1._numberOfWheels; 
	_primaryColor = car1._primaryColor; _material = car1._material; 	
	_carOccupants = car1._carOccupants; _specialSkill = car1._specialSkill; 
	_currentPetrol = car1._currentPetrol; _currentPosition = car1._currentPosition; 
	_currentSpeed = car1._currentSpeed; _currentGear = car1._currentGear; 	
	_currentAngleSteering = car1._currentAngleSteering; _engineStarted = car1._engineStarted;
	std::cout << "[Copy constructor] Car number " << this->_numberId << " has been created using another car \n" << std::endl;
}

/**
* Destructor
*/
WackyRacerCar::~WackyRacerCar() {
	std::cout << "Car number " << _numberId << "is destroyed \n" << std::endl;
}

/**
* Internal method for computing the new petrol value
* @return true if the car can run. Otherwise, it returns false
*/
bool WackyRacerCar::computeNewPetrolValue() {
	bool carRuns=false;
	if (_currentPetrol > 0) {
		_currentPetrol--;
		carRuns = true;
	}
	return carRuns;	
}

/**
* Set the new current petrol value
* @param newPetrol is the new petrol value
*/
void WackyRacerCar::setCurrentPetrol(float newPetrol) {
	_currentPetrol = newPetrol;
}

/**
* Get the current petrol value
* @return the current petrol value
*/
float WackyRacerCar::getCurrentPetrol() {
	return _currentPetrol;
}

/**
* Get the current petrol value. The valus is returned by reference
* @param returningCurrentPetrol is the current petrol value returned
*/
void WackyRacerCar::getCurrentPetrol(float & returningCurrentPetrol) {
	returningCurrentPetrol = _currentPetrol;
}

/**
* Starts the engine
*/
void WackyRacerCar::startEngine() {
	std::cout << "Engine started \n" << std::endl;
}

/**
* Stops the engine
*/
void WackyRacerCar::stopEngine() {
	std::cout << "Engine stopped \n" << std::endl;
}

void WackyRacerCar::accelerate() {
}
void WackyRacerCar::brake() {
}
void WackyRacerCar::gearUp() {

}
void WackyRacerCar::gearDown() {
}
void WackyRacerCar::moveSteering(float angle) {

}

/*
* The program's execution starts here
*/
int main(int argc, char ** argv) {
		//Instance an empty WackyRacerCar object represented by the TurboTerrific variable
		//The object is built using the Default constructor, which is automatically called by the program
	WackyRacerCar turboTerrific;	
									
		//Instance a WackyRacerCar object represented by the MeanMachine variable
		//The object is built using the Overload constructor, which is automatically called by the program
	WackyRacerCar meanMachine(1, 4, PURPLE_COLOR, "metal", 2, "rockets", 100, 1);

		//Instance a WackyRacerCar object represented by the MeanMachine2 variable
		//The object is built using the copy constructor, which is automatically called by the program
	WackyRacerCar meanMachine2(meanMachine);

	float petrol = 0;
	meanMachine.getCurrentPetrol(petrol);
	std::cout << "The current petrol is " << petrol << "\n"<<std::endl;

	std::cout << "Introduce the new petrol's value: " << std::endl;
	std::cin >> petrol;
	meanMachine.setCurrentPetrol(petrol);
	
	meanMachine.startEngine();

	meanMachine.stopEngine();

	system("pause");

	return 0;
}

