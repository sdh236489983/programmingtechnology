#include <stdio.h>
#include <stdlib.h>

#define MAX_ROW 10
#define MAX_COLUMN 10

int main(int argc, char ** argv) {
	char ** a;
	a = (char **)malloc(sizeof(char*) * MAX_ROW);

	for (int i = 0; i < MAX_ROW; i++) {
		a[i] = (char *)malloc(sizeof(char) * MAX_COLUMN);
		printf("Block of memory %d \n", i);
		for (int j = 0; j < MAX_COLUMN; j++) {
			a[i][j] = '-';
			printf("@ memory of a[%d][%d] is %d and its value is %c  \n", i, j, &a[i][j], a[i][j]);
		}		
		
	}

	system("pause");

	return 0;
}