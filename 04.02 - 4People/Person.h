#pragma once
#include <string>
/*
* This class represents a person with the following data: age, sex and name
*/
class Person {
private:
	int _age;	
	char _sex;
	std::string _name;	
public:
		//Default constructor
	Person();
		//Overload constructor
	Person(Person & p);
		//Copy constructor
	Person(std::string name, int age, char sex);
		//Destructor
	~Person();

		//Get and Set methods for getting and setting the attribute values
	int getAge();
	void setAge(int age);
	char getSex();
	void setSex(char sex);
	std::string getName();
	void setName(std::string name);

		//In and output methods
	void printPerson();
	void askPersonData();
	
};

