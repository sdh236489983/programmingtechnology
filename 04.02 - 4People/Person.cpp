#include "Person.h"
#include <iostream>				//Management of cin and cout

/*
* Default constructor. It initializes the initial values
*/
Person::Person() {
	_name = "";
	_sex = '-';
	_age = 0;
}

/*
* Copy constructor
* @param p is the person object used to initialize the current object
*/
Person::Person(Person & p) {
	_name = p._name;
	_age = p._age;
	_sex = p._sex;
}

/*
* Overload constructor
* @param name is the name of the person
* @param age is the age of the person
* @param sex is the sex of the person
*/
Person::Person(std::string name, int age, char sex) {
	_name = name;
	_age = age;
	_sex = sex;
}

/**
* Destructor. In this case the destructor is useless because we are not allocating memory
*/
Person::~Person() {	
}

/*
* Gets the age value
* @return the age of the person
*/
int Person::getAge() { 
	return _age; 
}

/*
* Sets the new age value
* @param age is the new value of the age
*/
void Person::setAge(int age) {
	_age = age;
}

/*
* Gets the sex value
* @return the sex value
*/
char Person::getSex() { 
	return _sex; 
}

/*
* Sets the new sex value
* @param sex is the new value of the sex
*/
void Person::setSex(char sex) {
	_sex = sex;
}

/*
* Gets the name of the person
* @return the name value
*/
std::string Person::getName() {
	return _name; 
}

/*
* Sets the new name of the person
* @param name is the new value of the name
*/
void Person::setName(std::string name) {
	_name = name;
}

/*
* Print the person data
*/
void Person::printPerson() {
	std::cout << _name << " - " << _age << " - " << _sex << std::endl;
}

/*
* Fill the person's data
*/
void Person::askPersonData() {
	std::cout << "What's your name?";
	std::cin >> _name;
	std::cout << "How old are you?";
	std::cin >> _age;
	std::cout << "What's your sex?";
	std::cin >> _sex;
}
