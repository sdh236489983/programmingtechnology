#include <stdio.h>
#include <stdlib.h>


/** 
* This method allocates 10 int in the STACK
* a is a static array so you don't have to unallocate the memory
*/
void allocatingStaticMemory() {
	int a[10];

	for (int i = 0; i < 10; i++) {
		a[i] = i;
		printf("@ memory of a[%d] is %d and its value is %d  \n", i, a + i, a[i]);
	}


}

/**
* This method allocates 10 int in the HEAP
* a is a dynamic array so you have to unallocate the memory
*/
void allocatingDynamicMemory() {
	int * a;
	//malloc is the original C operator for allocating memory
		//a = (int *)malloc(sizeof(int) * 10);
	//"New" is the evolution of the malloc operator introduced in C++ 
	a = new int[10];

	for (int i = 0; i < 10; i++) {
		a[i] = i;
		printf("@ memory of a[%d] is %d and its value is %d  \n", i, a + i, a[i]);
	}
	//free is the original C operator for unallocating memory 
		//free(a);
	//"Delete" is the evolution of the free operator introduced in C++
	//Do you remember what's the difference between "delete []" a and "delete a"?
	delete [] a;
}

/**
* This program allocates two arrays in memory. Notice the different range of the @ memories
*/
int main(int argc, char ** argv) {	
	allocatingStaticMemory();
	allocatingDynamicMemory();

	system("pause");

	return 0;
}

